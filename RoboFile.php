<?php

/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see http://robo.li/
 */
class RoboFile extends \Robo\Tasks
{
    public function generateDoc()
    {
        $this->taskGenDoc('doc/module.md')
            ->docClass('Codeception\Module\Nette')
            ->prepend('## Actions')
            ->processClassSignature(false)
            ->processProperty(false)
            ->processMethodSignature(false)
            ->reorderMethods('sort')
            ->filterMethods(function (\ReflectionMethod $method) {
                if (!$method->isPublic() || $method->isConstructor() || $method->isDestructor()) {
                    return false;
                }
                return strpos($method->getName(), '_') !== 0;
            })
            ->processMethod(function (\ReflectionMethod $method, $text) {
                $title = "### {$method->getName()}";
                if (!trim($text)) {
                    return $title . "__not documented__\n";
                }
                $text = str_replace('@part ', ' * `part` ', $text);
                $text = preg_replace('~@return (.*?)~', ' * `return` $1', $text);
                return "$title\n$text";
            })
            ->run();
    }
}