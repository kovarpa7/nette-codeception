<?php declare(strict_types=1);

namespace Codeception\Lib\Connector;

use Codeception\Lib\Connector\Nette\Environment;
use Codeception\Lib\Connector\Nette\Http\HttpResponse;
use Exception;
use Nette\Application\Application;
use Nette\DI\Container;
use Nette\DI\MissingServiceException;
use Nette\Http\IResponse;
use Symfony\Component\BrowserKit\Client;
use Symfony\Component\BrowserKit\Request;
use Symfony\Component\BrowserKit\Response;

class Nette extends Client
{
    /** Name of http request in container. */
    const HTTP_REQUEST = 'http.request';

    /** @var Container */
    private $container;
    /** @var Environment */
    private $environment;
    /** @var array */
    private $services = [];

    /** @var array */
    private $configuratorParams = [];

    /**
     * Construct Nette connector.
     *
     * @param Environment $env
     */
    public function __construct(Environment $env)
    {
        parent::__construct();
        $this->environment = $env;

        $this->followRedirects();
        $this->initialize();
    }

    /**
     * Initialize connector.
     */
    private function initialize(): void
    {
        // Create temp folder
        $this->environment->createTempDirectory();

        // Set default server params
        $_SERVER = array_merge($_SERVER, $this->environment->getServerParams());

        // Create initial container
        $this->bootApplication();

        // Call modules init listener
        foreach ($this->environment->getModules() as $module) {
            $module->onConnectorInitialize($this);
        }
    }

    /**
     * Initialize configurator.
     */
    private function bootApplication(): void
    {
        // Create configurator
        $configurator = $this->environment->initializeConfigurator($this->configuratorParams);

        // Call modules boot listener
        foreach ($this->environment->getModules() as $module) {
            $module->onApplicationBoot($this);
        }

        // Set persistent services
        $configurator->addServices($this->services);

        // Build container
        $this->container = $configurator->createContainer();
    }

    /**
     * Sets additional configurator parameters and reboot container.
     *
     * @param array $config
     */
    public function setConfiguratorParameters(array $config): void
    {
        // Sets user parameters
        $this->configuratorParams = array_merge($this->configuratorParams, $config);

        // Reboot container
        $this->bootApplication();
    }

    /**
     * Process HTTP request.
     *
     * @param Request $request request
     * @return Response response
     * @throws Exception
     */
    protected function doRequest($request)
    {
        // Set super global variables
        $method = strtoupper($request->getMethod());

        $_SERVER = array_merge($this->environment->getServerParams(), $request->getServer());
        $_SERVER['REQUEST_URI'] = $this->parseRequestUri($request->getUri());
        $_SERVER['REQUEST_METHOD'] = $method;

        if (in_array($method, ['GET', 'HEAD'])) {
            $_GET = $request->getParameters();
            $_POST = [];
        } else {
            $_POST = $request->getParameters();
            $_GET = [];
        }

        $_COOKIE = $request->getCookies();
        $_FILES = $this->convertFileUploads($request->getFiles());

        // Recreate container if any request is created before
        if ($this->getServiceByNameIfExist(self::HTTP_REQUEST)) {
            $this->bootApplication();
        }

        // Run application
        try {
            ob_start();
            $this->getServiceByType(Application::class)->run();
            $content = ob_get_clean();
        } catch (Exception $e) {
            ob_end_clean();
            throw $e;
        }

        /** @var HttpResponse $response */
        $response = $this->getServiceByType(IResponse::class);
        return $response->toBrowserResponse($content);
    }

    /**
     * Parse path from request URL.
     *
     * @param string $url
     * @return string
     */
    public function parseRequestUri(string $url): string
    {
        preg_match(',http[s]?://[a-zA-Z]*(/.*),i', $url, $matches);
        return $matches[1];
    }

    /**
     * Convert file upload representation for Nette.<br>
     * Symfony crawler returns size and error code as strings but Nette requires them as int due strict comparison.
     *
     * @param array $fileUpload
     * @return array
     */
    private function convertFileUploads(array $fileUpload): array
    {
        foreach ($fileUpload as $name => $arr) {
            foreach ($arr as $key => $val) {
                if ($key === 'size' || $key === 'error') {
                    $fileUpload[$name][$key] = (int)$fileUpload[$name][$key];
                }
            }
        }
        return $fileUpload;
    }

    /**
     * Retrieve an instance of class from the DI container by class name.
     *
     * @param string $class name of class to retrieve
     * @param bool $throw throw exception if service is not found
     * @return object|null returns null when container is not initialized or when $throw parameter is set to false and service is not found, otherwise found service
     * @throws MissingServiceException when $throw parameter is set to true and service is not found in container
     */
    public function getServiceByType(string $class, bool $throw = false)
    {
        return $this->container ? $this->container->getByType($class, $throw) : null;
    }

    /**
     * Retrieve an instance of class from the DI container by service name.
     *
     * @param string $serviceName name of service to retrieve
     * @return object|null null if container is not initialized otherwise found service
     * @throws MissingServiceException when service is not found in container
     */
    public function getServiceByName(string $serviceName)
    {
        return $this->container ? $this->container->getService($serviceName) : null;
    }

    /**
     * Retrieve an instance of class from DI container by service name if service exists.
     *
     * @param string $name
     * @return object|null
     */
    public function getServiceByNameIfExist(string $name)
    {
        if (!$this->container) {
            return null;
        }
        return $this->container->hasService($name) ? $this->container->getService($name) : null;
    }

    /**
     * Adds the service to DI container.
     *
     * @param string $name
     * @param object $object
     */
    public function addService(string $name, $object): void
    {
        $this->services[$name] = $object;

        if ($this->container) {
            $this->container->addService($name, $object);
        }
    }

    /**
     * Gets connector {@link Environment}.
     *
     * @return Environment
     */
    public function getEnvironment(): Environment
    {
        return $this->environment;
    }

    /**
     * Retrieve service from DI container (if exists) and make it as persistent for this connector.
     *
     * @param string $serviceName
     */
    public function persistService(string $serviceName): void
    {
        $service = $this->getServiceByNameIfExist($serviceName);
        if ($service) {
            $this->services[$serviceName] = $service;
        }
    }

    /**
     * Close connector.
     */
    public function close(): void
    {
        // Call modules
        foreach ($this->environment->getModules() as $module) {
            $module->onConnectorClose($this);
        }

        // Clean temp folder
        $this->environment->deleteTempDirectory();
    }
}