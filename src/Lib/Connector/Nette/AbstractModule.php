<?php declare(strict_types=1);

namespace Codeception\Lib\Connector\Nette;

use Codeception\Lib\Connector\Nette;
use Nette\DI\ContainerBuilder;

abstract class AbstractModule
{
    public function onConnectorInitialize(Nette $connector): void
    {
    }

    public function onConnectorClose(Nette $connector): void
    {
    }

    public function onApplicationBoot(Nette $connector): void
    {
    }

    public function onContainerCompile(ContainerBuilder $builder): void
    {
    }
}