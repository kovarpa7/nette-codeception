<?php declare(strict_types=1);

namespace Codeception\Lib\Connector\Nette\Cache;

use Codeception\Lib\Connector\Nette;
use Codeception\Lib\Connector\Nette\AbstractModule;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Nette\Caching\Storages\IJournal;
use Nette\Caching\Storages\SQLiteJournal;

final class CacheModule extends AbstractModule
{
    /** Name of cache storage service. */
    const STORAGE_SERVICE = 'cache.storage';
    /** Name of cache journal service. */
    const JOURNAL_SERVICE = 'cache.journal';

    /** @var \ReflectionProperty */
    private $journalReflection;

    public function __construct()
    {
        $this->journalReflection = new \ReflectionProperty(SQLiteJournal::class, 'pdo');
        $this->journalReflection->setAccessible(true);
    }

    public function onApplicationBoot(Nette $connector): void
    {
        $connector->persistService(self::STORAGE_SERVICE);
        $connector->persistService(self::JOURNAL_SERVICE);
    }

    public function onConnectorClose(Nette $connector): void
    {
        /** @var IStorage|null $storage */
        $storage = $connector->getServiceByNameIfExist(self::STORAGE_SERVICE);
        if ($storage) {
            $storage->clean([Cache::ALL => true]);
        }

        /** @var IJournal|null $journal */
        $journal = $connector->getServiceByNameIfExist(self::JOURNAL_SERVICE);
        if ($journal && $journal instanceof SQLiteJournal) {
            $this->journalReflection->setValue($journal, null);
        }
    }
}