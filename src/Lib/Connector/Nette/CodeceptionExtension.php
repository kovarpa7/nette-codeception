<?php declare(strict_types=1);

namespace Codeception\Lib\Connector\Nette;

use Nette\DI\CompilerExtension;

class CodeceptionExtension extends CompilerExtension
{
    /** @var AbstractModule[] */
    private $modules;

    /**
     * @param AbstractModule[] $modules
     */
    public function __construct(array $modules)
    {
        $this->modules = $modules;
    }

    public function beforeCompile(): void
    {
        foreach ($this->modules as $module) {
            $module->onContainerCompile($this->getContainerBuilder());
        }
    }
}