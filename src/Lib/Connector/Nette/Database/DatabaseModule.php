<?php declare(strict_types=1);

namespace Codeception\Lib\Connector\Nette\Database;

use Codeception\Lib\Connector\Nette;
use Codeception\Lib\Connector\Nette\AbstractModule;
use Nette\Database\Connection;
use Nette\DI\ContainerBuilder;

final class DatabaseModule extends AbstractModule
{
    /** Name of nette database service. */
    const CONNECTION_SERVICE = 'database.default.connection';

    public function onConnectorInitialize(Nette $connector): void
    {
        if ($connector->getEnvironment()->isCleanupEnabled()) {
            /** @var Connection|null $database */
            $database = $connector->getServiceByName(self::CONNECTION_SERVICE);
            if ($database) {
                $database->beginTransaction();
            }
        }
    }

    public function onApplicationBoot(Nette $connector): void
    {
        $connector->persistService(self::CONNECTION_SERVICE);
    }

    public function onContainerCompile(ContainerBuilder $builder): void
    {
        $con = $builder->getByType(Connection::class) ?: self::CONNECTION_SERVICE;
        if ($builder->hasDefinition($con)) {
            $def = $builder->getDefinition($con);
            $args = $def->getFactory()->arguments;

            $def->setFactory(TransactionConnection::class, $args);
        }
    }

    public function onConnectorClose(Nette $connector): void
    {
        /** @var Connection|null $database */
        $database = $connector->getServiceByNameIfExist(self::CONNECTION_SERVICE);
        if ($database) {
            if ($connector->getEnvironment()->isCleanupEnabled()) {
                $database->rollBack();
            }
            $database->disconnect();
        }
    }
}