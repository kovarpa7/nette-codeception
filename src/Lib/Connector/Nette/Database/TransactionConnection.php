<?php declare(strict_types=1);

namespace Codeception\Lib\Connector\Nette\Database;

use Nette\Database\Connection as NetteConnection;

class TransactionConnection extends NetteConnection
{
    /** @var int */
    private $transactionLevel = 0;

    public function beginTransaction(): void
    {
        $this->transactionLevel++;
        if ($this->transactionLevel === 1) {
            parent::beginTransaction();
        } else {
            $this->getPdo()->exec('SAVEPOINT ' . $this->getSavepointName());
        }
    }

    public function commit(): void
    {
        if ($this->transactionLevel === 1) {
            parent::commit();
        } else {
            $this->getPdo()->exec('RELEASE SAVEPOINT ' . $this->getSavepointName());
        }
        $this->transactionLevel--;
    }

    public function rollBack(): void
    {
        if ($this->transactionLevel === 1) {
            parent::rollBack();
        } else {
            $this->getPdo()->exec('ROLLBACK TO SAVEPOINT ' . $this->getSavepointName());
        }
        $this->transactionLevel--;
    }

    private function getSavepointName(): string
    {
        return 'NDBT_SP_' . $this->transactionLevel;
    }
}
