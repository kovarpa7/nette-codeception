<?php declare(strict_types=1);

namespace Codeception\Lib\Connector\Nette;

use Nette\Configurator;
use Nette\DI\Compiler;
use Nette\Utils\FileSystem;

class Environment
{
    /** @var string */
    private $tempDir;
    /** @var string|null */
    private $logDir;
    /** @var array */
    private $parameters;
    /** @var array */
    private $configFiles;
    /** @var array */
    private $config;
    /** @var bool */
    private $cleanup;
    /** @var string */
    private $timeZone;
    /** @var array */
    private $serverParams = [
        'SERVER_NAME' => 'localhost',
        'REMOTE_ADDR' => '127.0.0.1'
    ];
    /** @var string[] */
    private $extensions;
    /** @var AbstractModule[] */
    private $modules;

    /**
     * Construct environment.
     *
     * @param array $config
     * @param AbstractModule[] $modules
     */
    public function __construct(array $config, array $modules)
    {
        $appDir = $this->getPath($config, 'appDir');
        $wwwDir = $this->getPath($config, 'wwwDir');

        $this->tempDir = $this->getPath($config, 'tempDir');
        $this->logDir = $this->getPath($config, 'logDir');
        $this->parameters = array_merge($config['parameters'], ['appDir' => $appDir, 'wwwDir' => $wwwDir]);
        $this->cleanup = $config['cleanup'];
        $this->timeZone = $config['timeZone'];
        $this->configFiles = [];
        $this->config = $config['config'];
        $this->extensions = $config['extensions'];
        $this->modules = $modules;

        foreach ($config['configFiles'] as $file) {
            $this->configFiles[] = $appDir . DIRECTORY_SEPARATOR . $file;
        }

        if ($this->logDir !== null) {
            FileSystem::createDir($this->logDir);
        }
    }

    /**
     * Create temp folder.
     */
    public function createTempDirectory(): void
    {
        FileSystem::createDir($this->tempDir);
    }

    /**
     * Delete temp folder from filesystem.
     */
    public function deleteTempDirectory(): void
    {
        FileSystem::delete($this->tempDir);
    }

    /**
     * Initialize Nette bootstrap class.
     *
     * @param array $configuratorParams
     * @return Configurator
     */
    public function initializeConfigurator(array $configuratorParams): Configurator
    {
        $configurator = new Configurator();
        $configurator->addParameters($this->parameters);
        $configurator->addDynamicParameters($configuratorParams + [$this->createContainerHash() => true]);
        $configurator->setTempDirectory($this->tempDir);
        $configurator->setDebugMode(false);

        if ($this->timeZone !== null) {
            $configurator->setTimeZone($this->timeZone);
        }

        if ($this->logDir !== null) {
            $configurator->enableDebugger($this->logDir);
        }

        foreach ($this->configFiles as $configFile) {
            $configurator->addConfig($configFile);
        }

        $configurator->onCompile[] = function (Configurator $configurator, Compiler $compiler) {
            foreach ($this->extensions as $name => $class) {
                $compiler->addExtension($name, new $class);
            }

            $compiler->addConfig($this->config);
            $compiler->addExtension('codeception-modules', new CodeceptionExtension($this->modules));
        };

        return $configurator;
    }

    /**
     * Check if tests should run in database transaction.
     *
     * @return bool
     */
    public function isCleanupEnabled(): bool
    {
        return $this->cleanup;
    }

    /**
     * Gets default server parameters.
     *
     * @return array
     */
    public function getServerParams(): array
    {
        return $this->serverParams;
    }

    /**
     * Returns absolute path from Codeception root to requested directory.
     *
     * @param array $config
     * @param string $directory
     * @return string|null
     */
    private function getPath(array $config, string $directory): ?string
    {
        return isset($config[$directory]) ? codecept_root_dir($config[$directory]) : null;
    }

    /**
     * Generate unique hash for configurator settings.
     *
     * @return string
     */
    private function createContainerHash(): string
    {
        $hash = [$this->extensions, $this->config];
        return md5(serialize($hash));
    }

    /**
     * Gets Nette modules.
     *
     * @return AbstractModule[]
     */
    public function getModules(): array
    {
        return $this->modules;
    }
}