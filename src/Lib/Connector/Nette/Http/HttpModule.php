<?php declare(strict_types=1);

namespace Codeception\Lib\Connector\Nette\Http;

use Codeception\Lib\Connector\Nette;
use Codeception\Lib\Connector\Nette\AbstractModule;
use Nette\DI\ContainerBuilder;
use Nette\Http\IResponse;
use Nette\Http\Session as NetteSession;

final class HttpModule extends AbstractModule
{
    public function onContainerCompile(ContainerBuilder $builder): void
    {
        $session = $builder->getByType(NetteSession::class) ?: 'session.session';
        if ($builder->hasDefinition($session)) {
            $builder->getDefinition($session)
                ->setClass(NetteSession::class)
                ->setFactory(Session::class);
        }

        $response = $builder->getByType(IResponse::class) ?: 'httpResponse';
        if ($builder->hasDefinition($response)) {
            $builder->getDefinition($response)
                ->setClass(IResponse::class)
                ->setFactory(HttpResponse::class);
        }
    }

    public function onConnectorClose(Nette $connector): void
    {
        /** @var Session $session */
        $session = $connector->getServiceByType(Session::class);
        if ($session && $session->isStarted()) {
            $session->destroy();
        }
    }
}