<?php declare(strict_types=1);

namespace Codeception\Lib\Connector\Nette\Http;

use Nette\Http\Session as NetteSession;

class Session extends NetteSession
{
    public function setExpiration($time)
    {
        if (!$this->isStarted()) {
            return parent::setExpiration($time);
        }
        return $this;
    }
}