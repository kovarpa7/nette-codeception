<?php
namespace Codeception\Lib\Connector\Nette\Tracy;

use Tracy\ILogger;
use PHPUnit\Framework\Assert;

/**
 * Proxy wrapper for Tracy\Logger.
 * @package Codeception\Lib\Connector\Nette\Tracy
 */
class Logger implements ILogger
{
    /** @var ILogger */
    private $logger;

    /**
     * Logger constructor.
     * @param ILogger $logger
     */
    public function __construct(ILogger $logger)
    {
        $this->logger = $logger;
    }

    public function log($value, $priority = self::INFO)
    {
        $msg = $this->logger->log($value, $priority);
        Assert::fail($value);
        return $msg;
    }
}