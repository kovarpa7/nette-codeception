<?php declare(strict_types=1);

namespace Codeception\Lib\Connector\Nette\Tracy;

use Codeception\Lib\Connector\Nette;
use Codeception\Lib\Connector\Nette\AbstractModule;
use Nette\DI\ContainerBuilder;
use Tracy\Debugger;
use Tracy\ILogger;

final class TracyModule extends AbstractModule
{
    /** @var \Closure|null */
    private $closure;

    public function __construct()
    {
        $isAvailable = class_exists('Tracy\Debugger');
        if ($isAvailable) {
            // Initialize clean function
            $function = function () {
                Debugger::$blueScreen = null;
                Debugger::$bar = null;
                Debugger::$logger = null;
                Debugger::$fireLogger = null;
            };
            // Bind function to Debugger scope
            $this->closure = $function->bindTo(null, Debugger::class);
        }
    }

    public function onConnectorClose(Nette $connector): void
    {
        $this->clearDebuggerVariables();
    }

    public function onApplicationBoot(Nette $connector): void
    {
        $this->clearDebuggerVariables();
    }

    private function clearDebuggerVariables(): void
    {
        if ($this->closure !== null) {
            ($this->closure)();
        }
    }

    public function onContainerCompile(ContainerBuilder $builder): void
    {
        $tracy = $builder->getByType(ILogger::class) ?: 'tracy.logger';
        if ($builder->hasDefinition($tracy)) {
            $def = clone $builder->getDefinition($tracy);

            $builder->addDefinition('module.orig.logger', $def)
                ->setAutowired(false);

            $builder->getDefinition($tracy)
                ->setClass(ILogger::class)
                ->setFactory(Logger::class)
                ->setArguments(['@module.orig.logger']);
        }
    }
}
