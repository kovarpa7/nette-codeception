<?php declare(strict_types=1);

namespace Codeception\Module;

use Codeception\Lib\Connector\Nette as NetteConnector;
use Codeception\Lib\Connector\Nette\Environment;
use Codeception\Lib\Framework;
use Codeception\Lib\Interfaces\Db;
use Codeception\Lib\Interfaces\PartedModule;
use Codeception\TestInterface;
use Nette\Application\Application;
use Nette\Application\LinkGenerator;
use Nette\Application\UI\Form;
use Nette\ComponentModel\Container;
use Nette\Database\Context;
use Nette\Database\DriverException;
use Nette\Database\IRow;
use Nette\DI\MissingServiceException;
use Nette\Http\Session;
use Nette\Http\SessionSection;
use Nette\Security\Identity;
use Nette\Security\User;

class Nette extends Framework implements Db, PartedModule
{
    /** @var array */
    protected $config = [
        'appDir' => 'app',
        'wwwDir' => 'www',
        'logDir' => null,
        'parameters' => [],
        'timeZone' => null,
        'configFiles' => [],
        'config' => [],
        'extensions' => [],
        'cleanup' => true,
        'modules' => [
            NetteConnector\Http\HttpModule::class,
            NetteConnector\Database\DatabaseModule::class,
            NetteConnector\Cache\CacheModule::class,
            NetteConnector\Tracy\TracyModule::class
        ]
    ];
    /** @var array */
    protected $requiredFields = ['tempDir'];

    /** @var NetteConnector\AbstractModule[] */
    protected $modules = [];
    /** @var Environment */
    protected $environment;

    /** @var NetteConnector */
    public $client;

    /**
     * Initialize hook.
     */
    public function _initialize()
    {
        parent::_initialize();

        foreach ($this->config['modules'] as $module) {
            $this->modules[] = new $module();
        }

        $this->environment = new Environment($this->config, $this->modules);
    }

    /**
     * Before hook.
     *
     * @param TestInterface $test
     */
    public function _before(TestInterface $test)
    {
        $this->client = new NetteConnector($this->environment);
    }

    /**
     * After hook.
     *
     * @param TestInterface $test
     */
    public function _after(TestInterface $test)
    {
        // Close connector
        if ($this->client !== null) {
            $this->client->close();
            $this->client = null;
        }

        // Clear super global variables
        $_REQUEST = $_SESSION = $_GET = $_POST = $_FILES = $_COOKIE = [];
        parent::_after($test);
    }

    public function _parts()
    {
        return ['db', 'services'];
    }

    /**
     * Inserts an SQL record into a database.
     *
     * ```php
     * <?php
     * $I->haveInDatabase('users', array('name' => 'miles', 'email' => 'miles@davis.com'));
     * ?>
     * ```
     *
     * @param string $table
     * @param array $data
     * @return int|bool|IRow Returns IRow or number of affected rows for Selection or table without primary key
     * @part db
     */
    public function haveInDatabase(string $table, array $data)
    {
        $this->assertTrue(class_exists(Context::class), 'Nette database package missing so you cannot use database actions.');

        /** @var Context $database */
        $database = $this->grabService(Context::class);
        try {
            return $database->table($table)->insert($data);
        } catch (DriverException $e) {
            $this->fail($e->getMessage());
            return null;
        }
    }

    /**
     * Asserts that a row with the given column values exists.
     * Provide table name and column values.
     *
     * ``` php
     * <?php
     * $I->seeInDatabase('users', array('name' => 'Davert', 'email' => 'davert@mail.com'));
     * ```
     * Fails if no such user found.
     *
     * @param string $table
     * @param array $criteria
     * @part db
     */
    public function seeInDatabase($table, $criteria = [])
    {
        $result = $this->countInDatabase($table, $criteria);
        $this->assertGreaterThan(0, $result, 'No matching records found for criteria ' . json_encode($criteria) . ' in table ' . $table);
    }

    /**
     * Effect is opposite to ->seeInDatabase
     *
     * Asserts that there is no record with the given column values in a database.
     * Provide table name and column values.
     *
     * ``` php
     * <?php
     * $I->dontSeeInDatabase('users', array('name' => 'Davert', 'email' => 'davert@mail.com'));
     * ```
     * Fails if such user was found.
     *
     * @param string $table
     * @param array $criteria
     * @part db
     */
    public function dontSeeInDatabase($table, $criteria = [])
    {
        $result = $this->countInDatabase($table, $criteria);
        $this->assertLessThan(1, $result, 'Unexpectedly found matching records for criteria ' . json_encode($criteria) . ' in table ' . $table);
    }

    /**
     * Fetches a single column value from a database.
     * Provide table name, desired column and criteria.
     *
     * ``` php
     * <?php
     * $mail = $I->grabFromDatabase('users', 'email', array('name' => 'Davert'));
     * ```
     *
     * @param string $table
     * @param string $column
     * @param array $criteria
     *
     * @return mixed
     * @part db
     */
    public function grabFromDatabase($table, $column, $criteria = [])
    {
        return $this->proceedGrabDatabase($table, $column, $criteria);
    }

    /**
     * Count rows in database.
     *
     * @param string $table
     * @param array $criteria
     * @return int
     */
    protected function countInDatabase(string $table, array $criteria): int
    {
        return (int)$this->proceedGrabDatabase($table, 'COUNT(*)', $criteria);
    }

    /**
     * Fetch field from database.
     *
     * @param string $table
     * @param string $column
     * @param array $criteria
     * @return mixed
     */
    protected function proceedGrabDatabase(string $table, string $column, array $criteria)
    {
        $this->assertTrue(class_exists(Context::class), 'Nette database package missing so you cannot use database actions.');

        $database = $this->grabService(Context::class);
        return $database->table($table)->where($criteria)->select($column)->fetchField();
    }

    /**
     * Grabs service from DI container by class name.
     *
     * Example
     * ``` php
     * <?php
     *
     * $I->grabService(Foo::class)
     *
     * // Will return an instance of Foo.
     * ?>
     * ```
     *
     * @param string $class
     * @return object
     * @part services
     */
    public function grabService(string $class)
    {
        try {
            return $this->client->getServiceByType($class, true);
        } catch (MissingServiceException $e) {
            $this->fail($e->getMessage());
        }
        return null;
    }

    /**
     * Grabs service from DI container by service name.
     *
     * Example
     * ``` php
     * <?php
     *
     * $I->grabServiceByName('database.default.connection');
     *
     * // Will return service with name 'database.default.connection'.
     * ?>
     * ```
     *
     * @param string $name
     * @return object
     * @part services
     */
    public function grabServiceByName(string $name)
    {
        try {
            return $this->client->getServiceByName($name);
        } catch (MissingServiceException $e) {
            $this->fail($e->getMessage());
        }
        return null;
    }

    /**
     * Adds the service to DI container.
     *
     * Example
     * ``` php
     * <?php
     *
     * $I->addService('my.service', new Foo());
     *
     * // Will register instance of Foo to DI container as service with name 'my.service'.
     * ?>
     * ```
     *
     * @param string $name
     * @param object $object
     * @part services
     */
    public function addService(string $name, $object): void
    {
        $this->client->addService($name, $object);
    }

    /**
     * Assert that session variable exists.
     *
     * Example
     * ``` php
     * <?php
     *
     * $I->seeInSession('user', 'data');
     *
     * // Will check that key 'data' exist in session section 'user'.
     *
     * $I->seeInSession('user', 'id', 1);
     *
     * // Will check that value of key 'id' at session section 'user' is set to '1'.
     * ?>
     * ```
     *
     * @param string $section
     * @param string $key
     * @param mixed|null $value
     */
    public function seeInSession(string $section, string $key, $value = null): void
    {
        $section = $this->getSessionSection($section);
        if (!isset($section[$key])) {
            $this->fail("No session variable found for section '$section' and key '$key'.");
        }
        if ($value !== null) {
            $this->assertEquals($value, $section[$key], "Session value '$section[$key]' do not match with expected value '$value'.");
        }
    }

    /**
     * Effect is opposite to ->seeInSession.
     *
     * Assert that session variable do not exist.
     *
     * Example
     * ``` php
     * <?php
     *
     * $I->dontSeeInSession('user', 'id');
     *
     * // Will check that key 'id' is not set in session section 'user'.
     * ?>
     * ```
     *
     * @param string $section
     * @param string $key
     */
    public function dontSeeInSession(string $section, string $key): void
    {
        $section = $this->getSessionSection($section);
        if (isset($section[$key])) {
            $this->fail("No session variable found for section '$section' and key '$key'.");
        }
    }

    /**
     * Returns session section with requested name.
     *
     * @param string $section
     * @return SessionSection
     */
    private function getSessionSection(string $section): SessionSection
    {
        /** @var Session $session */
        $session = $this->grabService(Session::class);
        return $session->getSection($section);
    }


    /**
     * Opens page for given route.
     *
     * Example
     * ``` php
     * <?php
     *
     * $I->amOnRoute('Homepage:default');
     *
     * // Will render default action of homepage presenter.
     *
     * $I->amOnRoute('Homepage:say', ['text' => 'Hello world!'];
     *
     * // Will pass 'text' parameter to say action of homepage presenter and render it.
     *
     * ?>
     * ```
     *
     * @param string $dest
     * @param array $args
     * @throws \Nette\Application\UI\InvalidLinkException
     */
    public function amOnRoute(string $dest, array $args = []): void
    {
        $page = $this->getRouteByName($dest, $args);
        $this->amOnPage($page);
    }

    /**
     * Checks that the current route is equal to given route.
     *
     * Example
     * ```php
     * <?php
     *
     * $I->seeCurrentRouteEquals('Homepage:default');
     *
     * // Will check that current route is 'Homepage:default'.
     *
     * $I->seeCurrentRouteEquals('Homepage:say', ['text' => 'Hello world!']);
     *
     * // Will check that current route is 'Homepage:default' with parameter 'text' => 'Hello world!'.
     *
     * ?>
     * ```
     *
     * @param string $dest
     * @param array $args
     * @throws \Nette\Application\UI\InvalidLinkException
     */
    public function seeCurrentRouteEquals(string $dest, array $args = []): void
    {
        $page = $this->getRouteByName($dest, $args);
        $this->seeCurrentUrlEquals($page);
    }

    /**
     * Effect opposite to ->seeCurrentRouteEquals.
     *
     * Checks that the current route is not equal to given route.
     *
     * Example
     * ```php
     * <?php
     *
     * $I->dontSeeCurrentRouteEquals('Homepage:default');
     *
     * // Will check that current route is not 'Homepage:default'.
     *
     * $I->dontSeeCurrentRouteEquals('Homepage:say', ['text' => 'Hello world!']);
     *
     * // Will check that current route is not 'Homepage:default' with parameter 'text' => 'Hello world!'.
     *
     * ?>
     * ```
     *
     * @param string $dest
     * @param array $args
     * @throws \Nette\Application\UI\InvalidLinkException
     */
    public function dontSeeCurrentRouteEquals(string $dest, array $args = []): void
    {
        $page = $this->getRouteByName($dest, $args);
        $this->dontSeeCurrentUrlEquals($page);
    }

    /**
     * Check if current URL starts with URL generated from given route.
     *
     * Example
     * ```php
     * <?php
     *
     * $I->amOnRoute('Homepage:default', ['number1' => 42, 'number2' => 24]);
     *
     * // Will open page '/homepage/default?number1=42&number2=24'
     *
     * $I->seeInCurrentRoute('Homepage:default', ['number1' => 42]);
     *
     * // Will check that URL starts with '/homepage/default?number1=42'
     *
     * $I->seeInCurrentRoute('Homepage:default', ['number2' => 24]);
     *
     * // Will FAIL because URL do not start with '/homepage/default?number2=24'
     * ?>
     * ```
     *
     * @param string $dest
     * @param array $args
     * @throws \Nette\Application\UI\InvalidLinkException
     */
    public function seeInCurrentRoute(string $dest, array $args = []): void
    {
        $page = $this->getRouteByName($dest, $args);
        $this->seeInCurrentUrl($page);
    }

    /**
     * Effect opposite to ->seeInCurrentRoute.
     *
     * Check if current URL do not start with URL generated from given route.
     *
     * Example
     * ```php
     * <?php
     *
     * $I->amOnRoute('Homepage:default', ['number1' => 42, 'number2' => 24]);
     *
     * // Will open page '/homepage/default?number1=42&number2=24'
     *
     * $I->dontSeeInCurrentRoute('Homepage:default', ['number2' => 24]);
     *
     * // Will check that URL do not start with '/homepage/default?number2=24'
     *
     * $I->dontSeeInCurrentRoute('Homepage:default', ['number1' => 42]);
     *
     * // Will FAIL because URL start with '/homepage/default?number1=42'
     * ?>
     * ```
     *
     * @param string $dest
     * @param array $args
     * @throws \Nette\Application\UI\InvalidLinkException
     */
    public function dontSeeInCurrentRoute(string $dest, array $args = []): void
    {
        $page = $this->getRouteByName($dest, $args);
        $this->dontSeeInCurrentUrl($page);
    }

    /**
     * Translate route to URL path.
     *
     * @param string $dest
     * @param array $args
     * @return string
     * @throws \Nette\Application\UI\InvalidLinkException
     */
    private function getRouteByName(string $dest, array $args = []): string
    {
        /** @var LinkGenerator $generator */
        $generator = $this->grabService(LinkGenerator::class);

        $url = $generator->link($dest, $args);
        return $this->client->parseRequestUri($url);
    }

    /**
     * Assert that form with given name have at least one error.
     *
     * Example
     * ``` php
     * <?php
     *
     * $I->seeFormHasErrors('loginForm');
     *
     * // Will check if form component with name 'loginForm' have any error.
     * ?>
     * ```
     *
     * @param string $formName
     */
    public function seeFormHasErrors(string $formName): void
    {
        $form = $this->findForm($formName);
        $this->assertTrue($form->hasErrors(), "Form named '$formName' do not have any error.");
    }

    /**
     * Effect is opposite to ->seeFormHasErrors.
     *
     * Assert that form with given name do not have any error.
     *
     * Example
     * ``` php
     * <?php
     *
     * $I->dontSeeFormHasErrors('loginForm');
     *
     * // Will check if form component with name 'loginForm' do not have any error.
     * ?>
     * ```
     *
     * @param string $formName
     */
    public function dontSeeFormHasErrors(string $formName): void
    {
        $form = $this->findForm($formName);
        $this->assertFalse($form->hasErrors(), "Form named '$formName' have at least one error.");
    }

    /**
     * Find form component with given name at last presenter.
     *
     * @param string $formName
     * @return Form instance of Form component if found otherwise fail assertion will be fired
     */
    private function findForm(string $formName): Form
    {
        /** @var Application $app */
        $app = $this->grabService(Application::class);
        $presenter = $app->getPresenter();

        if (!($presenter instanceof Container)) {
            $this->fail('Presenter should be instance of ' . Container::class . ' get ' . get_class($presenter));
        }

        // Strip prefix from name if set
        if (strpos($formName, '#frm-') === 0) {
            $formName = substr($formName, 5);
        }

        // Find component
        $components = $presenter->getComponents(true, Form::class);
        /** @var Form $component */
        foreach ($components as $component) {
            if ($component->getName() === $formName) {
                return $component;
            }
        }
        $this->fail("Form named '$formName' not found.");
        return null;
    }

    /**
     * Sets additional application parameters.
     *
     * Example
     * ``` php
     * <?php
     *
     * $I->haveApplicationParameters(['dataDir' => '/e/t/c'])
     *
     * ?>
     * ```
     *
     * @param array $config
     * @part services
     */
    public function haveApplicationParameters(array $config = []): void
    {
        $this->client->setConfiguratorParameters($config);
    }

    /**
     * Changes currently logged user to application.
     *
     * Example
     * ``` php
     * <?php
     *
     * $I->amLoggedInAs(new Identity(1, 'root'));
     *
     * // Will login user with id '1' and role 'root'.
     * ?>
     * ```
     *
     * @param Identity $identity
     * @throws \Nette\Security\AuthenticationException
     */
    public function amLoggedInAs(Identity $identity): void
    {
        $this->assertNotNull($identity, 'User identity cannot be null.');
        $this->getUser()->login($identity);
    }

    /**
     * Logout user from application.
     *
     * Example
     * ``` php
     * <?php
     *
     * $I->logout();
     *
     * // Will logout user from application.
     * ?>
     * ```
     */
    public function logout(): void
    {
        $this->getUser()->logout(true);
    }

    /**
     * Assert that user is authenticated.
     *
     * Example
     * ``` php
     * <?php
     *
     * $I->seeAuthenticated();
     *
     * // Will check that user is logged into application.
     * ?>
     * ```
     */
    public function seeAuthenticated(): void
    {
        $this->assertTrue($this->getUser()->isLoggedIn(), 'User is not logged in.');
    }

    /**
     * Effect opposite to ->seeAuthenticated.
     *
     * Assert that user is not authenticated.
     *
     * Example
     * ``` php
     * <?php
     *
     * $I->dontSeeAuthenticated();
     *
     * // Will check that user is not logged into application.
     * ?>
     * ```
     */
    public function dontSeeAuthenticated(): void
    {
        $this->assertFalse($this->getUser()->isLoggedIn(), 'User is logged in.');
    }

    /**
     * Retrieve user instance from DI container.
     *
     * @return User
     */
    private function getUser(): User
    {
        /** @var User $user */
        $user = $this->grabService(User::class);
        return $user;
    }
}