--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.4
-- Dumped by pg_dump version 9.5.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = ON;
SET check_function_bodies = FALSE;
SET client_min_messages = WARNING;
SET row_security = OFF;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = FALSE;

--
-- Name: user; Type: TABLE; Schema: public; Owner: tester
--

DROP TABLE IF EXISTS "user";
CREATE TABLE "user" (
  id        INTEGER                                               NOT NULL,
  username  CHARACTER VARYING(50) DEFAULT '' :: CHARACTER VARYING NOT NULL,
  firstname CHARACTER VARYING(50) DEFAULT '' :: CHARACTER VARYING NOT NULL,
  lastname  CHARACTER VARYING(50) DEFAULT '' :: CHARACTER VARYING NOT NULL
);


ALTER TABLE "user"
  OWNER TO tester;

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: tester
--

CREATE SEQUENCE user_id_seq
START WITH 1
INCREMENT BY 1
NO MINVALUE
NO MAXVALUE
CACHE 1;


ALTER TABLE user_id_seq
  OWNER TO tester;

--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tester
--

ALTER SEQUENCE user_id_seq OWNED BY "user".id;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: tester
--

ALTER TABLE ONLY "user"
  ALTER COLUMN id SET DEFAULT nextval('user_id_seq' :: REGCLASS);

--
-- Name: user_id; Type: CONSTRAINT; Schema: public; Owner: tester
--

ALTER TABLE ONLY "user"
  ADD CONSTRAINT user_id PRIMARY KEY (id);


INSERT INTO "user" (username, firstname, lastname) VALUES
  ('novakjan', 'Jan', 'Novák'),
  ('kucerev', 'Eva', 'Kučerová'),
  ('horajan', 'Jan', 'Horák');

--
-- PostgreSQL database dump complete
--

