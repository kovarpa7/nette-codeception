<?php
namespace Test\App\Presenter;

use Nette\Application\UI\Form;
use Nette\Application\UI\Presenter;

class FormPresenter extends Presenter
{
    public function createComponentExampleForm()
    {
        $form = new Form();
        $form->addText('width', 'Width')
            ->setRequired(true)
            ->addRule(Form::INTEGER, 'Width should be integer.');
        $form->addText('height', 'Height')
            ->setRequired(true)
            ->addRule(Form::INTEGER, 'Height should be integer.');
        $form->addSubmit('submit', 'Submit');
        return $form;
    }
}