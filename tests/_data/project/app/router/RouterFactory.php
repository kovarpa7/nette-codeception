<?php
namespace Test\App\Router;

use Nette\Application\IRouter;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;

class RouterFactory
{
    /**
     * @return IRouter
     */
    public static function create()
    {
        $router = new RouteList();
        $router[] = new Route('<presenter>[/<action>[/<id>]]', 'Homepage:default');
        return $router;
    }
}