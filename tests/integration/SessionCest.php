<?php

use Nette\Http\Session;

class SessionCest
{
    public function testSession(IntegrationTester $I)
    {
        $I->wantToTest('session handler');

        /** @var Session $session */
        $session = $I->grabService(Session::class);
        $section = $session->getSection('testSection');
        $section->testKey = 'testValue';
        $section->sharedKey = 'sharedValue';

        $I->seeInSession('testSection', 'testKey', 'testValue');
        $I->seeInSession('testSection', 'sharedKey');

        $I->amOnPage('/homepage/page');
        $I->seeInSession('testSection', 'testKey', 'testValue');

        $I->dontSeeInSession('testSection', 'missingKey');
        $I->dontSeeInSession('missingSection', 'key');

        unset($section['testKey']);

        $I->dontSeeInSession('testSection', 'testKey');
    }

    public function testSessionShare(IntegrationTester $I)
    {
        $I->wantToTest('that session is not shared between tests');
        $I->dontSeeInSession('testSection', 'sharedKey');
    }
}
