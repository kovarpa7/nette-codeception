<?php

use Nette\Security\Identity;

class UserCest
{
    public function testUser(IntegrationTester $I)
    {
        $I->wantToTest('user login and logout');

        $I->dontSeeAuthenticated();
        $I->amLoggedInAs($this->getIdentity());
        $I->seeAuthenticated();

        $I->logout();
        $I->dontSeeAuthenticated();
    }

    public function testLoginWithoutLogout(IntegrationTester $I)
    {
        $I->wantToTest('login without logout');

        $I->dontSeeAuthenticated();
        $I->amLoggedInAs($this->getIdentity());
        $I->seeAuthenticated();
    }

    public function testIsLogged(IntegrationTester $I)
    {
        $I->wantToTest('that i am not logged in');

        $I->dontSeeAuthenticated();
    }

    public function testLoginBeforeRequest(IntegrationTester $I)
    {
        $I->wantToTest('login before first request is sent');

        $I->amLoggedInAs($this->getIdentity());
        $I->seeAuthenticated();

        $I->amOnPage('/homepage/default');
        $I->seeAuthenticated();
    }

    /**
     * @return Identity
     */
    private function getIdentity()
    {
        return new Identity(1, 'root');
    }
}
